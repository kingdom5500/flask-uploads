import gzip
from io import BytesIO

from flask import abort, Blueprint, send_file
from werkzeug.utils import secure_filename

from app.database import Upload

download_bp = Blueprint("download", __name__)


@download_bp.route("/<file_code>", methods=["GET"])
def download(file_code: str):

    upload_obj = Upload.query.get(file_code)

    if not upload_obj:
        return abort(404)

    file_data = gzip.decompress(
        upload_obj.get_content()
    )

    return send_file(
        BytesIO(file_data),
        mimetype=upload_obj.mime_type,
        as_attachment=True,
        attachment_filename=upload_obj.file_name
    )


@download_bp.route("/<file_code>.gz", methods=["GET"])
def download_gz(file_code: str):

    upload_obj = Upload.query.get(file_code)

    if not upload_obj:
        return abort(404)

    file_name = secure_filename(file_code)
    file_data = upload_obj.get_content()

    return send_file(
        BytesIO(file_data),
        mimetype="application/gzip",
        as_attachment=True,
        attachment_filename=file_name + ".gz"
    )
