import os
import random

from app.constants import RANDOM_ID_LENGTH, ID_CHARACTERS

ID_MAX = len(ID_CHARACTERS) ** RANDOM_ID_LENGTH - 1
ID_PAD_CHAR = ID_CHARACTERS[0]  # Pad with the lowest 'digit'.


def _to_base_x(value: int, alphabet: str) -> str:
    """
    A generic helper function to convert a positive integer to
    any base, given an alphabet to use for the result of it.

    Note: Due to the way this works, a float value could
        TECHNICALLY be passed as the `value` argument, but
        it would just be truncated to an integer anyway.

    :param value: A positive integer to convert.
    :param alphabet: The alphabet to use for conversion.
    :return: A string of the converted number.
    """

    base = len(alphabet)
    converted = ""

    while value:
        value, mod = divmod(int(value), base)
        converted = alphabet[mod] + converted

    return converted


def random_file_id() -> str:
    """
    Generate a (pseudo-random) string to be used as a
    file identifier. The return value of this function
    should always be checked with the database, as it's
    possible that duplicates are produced from this.

    :return: A pseudo-random file identifier string.
    """

    # Generate the random ID integer and convert it.
    id_as_int = random.randint(0, ID_MAX)
    file_id = _to_base_x(id_as_int, ID_CHARACTERS)

    # Pad the resulting ID if necessary, as we're
    # basically just working with glorified integers here.
    padded = "{file_id:{padding}<{length}}".format(
        file_id=file_id,
        padding=ID_PAD_CHAR,
        length=RANDOM_ID_LENGTH
    )

    return padded


def shorten_file_name(file_name: str, length: int, *, default: str = "unknown", truncate_default: bool = True) -> str:
    """
    Shorten a file name to a given length, while also
    keeping the file extension intact. This will return
    a default value if the extension itself is too long,
    or the file name given is empty.

    Examples:
        >>> file_name = "cute_kitten_eating_pizza.jpeg"
        >>> shorten_file_name(file_name, 12)
        'cute_ki.jpeg'
        >>> shorten_file_name(file_name, 4, default="default")
        'defa'
        >>> shorten_file_name(file_name, 4, truncate_default=False)
        'unknown'
        >>> shorten_file_name("", 12)
        'unknown'

    :param file_name: The file name to shorten.
    :param length: The length of the resulting string.
    :param default: The default value to use in case the
        file name can't be shortened properly.
    :param truncate_default: True if the default value
        should also be truncated to fit the size.
    :return: A shortened file name. (no shit)
    """

    base_name, extension = os.path.splitext(file_name)

    # If we have an empty or weird file name:
    if not file_name or len(extension) >= length:

        # Return the default if we don't want to truncate it.
        if not truncate_default:
            return default

        # Otherwise, make sure the rest of the function works with this.
        base_name, extension = default, ""

    # Truncate the file name and stick the extension back on.
    new_base = base_name[:length - len(extension)]
    return new_base + extension
