import gzip
import os

from flask import Blueprint, redirect, render_template, url_for
from werkzeug.utils import secure_filename

from app.constants import SAVE_LOCATION
from app.database import db, Upload
from app.forms import UploadForm, DownloadForm

index_bp = Blueprint("index", __name__)


@index_bp.route("/", methods=["GET", "POST"])
def index():

    upload_form = UploadForm()
    download_form = DownloadForm()

    if upload_form.validate_on_submit():
        # Find the filename and make it secure.
        file_name = secure_filename(upload_form.file.data.filename)
        mime_type = upload_form.file.data.mimetype
        file_id = upload_form.name.data

        # Add a new entry to the DB.
        upload_obj = Upload(file_name, mime_type, file_id)
        db.session.add(upload_obj)
        db.session.commit()

        # Save the file to disk (compressed)
        save_path = os.path.join(SAVE_LOCATION, upload_obj.id)
        file_data = upload_form.file.data.stream.read()  # EnterpriseEdition

        with gzip.open(save_path, "wb") as gz:
            gz.write(file_data)

    if download_form.validate_on_submit():
        # Redirect to the actual download page.
        file_name = secure_filename(download_form.file_code.data)

        if download_form.gzipped.data:
            url = url_for("download.download_gz", file_code=file_name)
        else:
            url = url_for("download.download", file_code=file_name)

        return redirect(url)

    return render_template(
        "index.html",
        upload_form=upload_form,
        download_form=download_form
    )
