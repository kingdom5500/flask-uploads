import os
from typing import Optional

from flask_sqlalchemy import SQLAlchemy

from app.constants import MAX_ID_LENGTH, MAX_FILE_NAME, SAVE_LOCATION
from app.utils import random_file_id, shorten_file_name

db = SQLAlchemy()

# According to RFC 4288, the maximum length for a MIME type
# is 127 bytes on both sections, plus the slash which makes
# a total of 255 bytes long. An unlikely case, but possible.
MIME_TYPE_SIZE = 255


class Upload(db.Model):

    id = db.Column(db.String(MAX_ID_LENGTH), primary_key=True)
    file_name = db.Column(db.String(MAX_FILE_NAME))
    mime_type = db.Column(db.String(MIME_TYPE_SIZE))

    def __init__(self, file_name, mime_type, file_id: str = None):
        self.file_name = shorten_file_name(file_name, MAX_FILE_NAME)
        self.mime_type = mime_type

        # I don't like this, but I also don't like sequential
        # IDs, nor would I like to hand out UUIDs to users :(
        if not file_id:
            file_id = random_file_id()
            while Upload.query.get(file_id):
                file_id = random_file_id()

        self.id = file_id

    def get_content(self) -> Optional[bytes]:
        """
        Return the content of an upload's corresponding .gz file,
        and decrement the remaining counter.

        :return: The bytes of the .gz file's content, or None if
            there was an error with the file.
        """

        # Read the file data and whatnot.
        file_path = os.path.join(SAVE_LOCATION, self.id)

        try:
            with open(file_path, "rb") as file:
                file_data = file.read()
        except FileNotFoundError:
            return None

        # Delete the file and stuff.
        os.remove(file_path)
        Upload.query.filter(
            Upload.id == self.id
        ).delete()

        # Make changes and return
        db.session.commit()
        return file_data
