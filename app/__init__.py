import logging
from logging import Formatter, FileHandler

from flask import Flask

from app.constants import CONFIG
from app.database import db
from app.views.index import index_bp
from app.views.download import download_bp


def create_app():
    app = Flask(__name__)
    app.config.from_object(CONFIG)

    # Set up proper:tm: logging for production:tm: webscale:tm: deployment:tm:
    if not app.debug:
        file_handler = FileHandler('error.log')
        file_handler.setFormatter(
            Formatter('%(asctime)s %(levelname)s: %(message)s [in %(pathname)s:%(lineno)d]')
        )

        app.logger.setLevel(logging.INFO)

        file_handler.setLevel(logging.INFO)
        app.logger.addHandler(file_handler)
        app.logger.info('errors')

    # Initialise the database
    with app.app_context():
        db.init_app(app)
        db.create_all()

    app.register_blueprint(index_bp)
    app.register_blueprint(download_bp)

    return app


if __name__ == '__main__':
    flask_app = create_app()
    flask_app.run(host='0.0.0.0', port=5000)
