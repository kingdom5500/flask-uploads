from flask_wtf import FlaskForm
from flask_wtf.file import FileField, FileRequired
from wtforms import BooleanField, StringField
from wtforms.validators import DataRequired, Length

from app.constants import MAX_ID_LENGTH
from app.database import Upload


class UploadForm(FlaskForm):
    file = FileField(
        "Upload a file",
        validators=[FileRequired()]
    )

    name = StringField(
        "File ID",
        validators=[Length(max=MAX_ID_LENGTH)]
    )

    def validate(self):
        # Basic validation tests.
        if not FlaskForm.validate(self):
            return False

        # We can make a default value later.
        if not self.name.data:
            return True

        # Check if the name is already taken.
        upload = Upload.query.get(self.name.data)
        if upload is not None:
            self.name.errors.append("File ID taken.")
            return False

        return True


class DownloadForm(FlaskForm):
    file_code = StringField(
        "File ID",
        validators=[DataRequired(), Length(max=16)]
    )

    gzipped = BooleanField(
        "Pull directly from the server (as a .gz file)"
    )
