import os

basedir, config_name = os.path.split(__file__)
CONFIG = __name__

# Configuration of the flask app itself.
DEBUG = os.environ.get("FLASK_ENV", "development") == "development"
SECRET_KEY = os.environ.get("SECRET_KEY", "rawr")

# Stuff for the file identifier generation.
RANDOM_ID_LENGTH = 6
MAX_ID_LENGTH = 16
ID_CHARACTERS = (
    "23456789"
    "ABCDEFGHJKLMNPQRSTUVWXYZ"
    "abcdefghijkmnpqrstuvwxyz"
)

# SQL shenanigans.
DATABASE_PATH = "../database.db"
SQLALCHEMY_DATABASE_URI = "sqlite:///" + DATABASE_PATH
SQLALCHEMY_TRACK_MODIFICATIONS = False

# File-related shenanigans.
MAX_FILE_NAME = 256
MAX_CONTENT_LENGTH = 64 * 1024 * 1024  # 64MB
SAVE_LOCATION = "uploads"
